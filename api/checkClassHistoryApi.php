<?php

header('Content-Type: application/json');
include('../config.php');

$conn = new mysqli($servername, $username, $password, $dbname);

$ID_class = $_GET['idClass'];
$ID_check = $_GET['idCheck'];

$sql = "SELECT Code,owner,color FROM class WHERE ID = '$ID_class'";
$result = $conn->query($sql)->fetch_assoc();
$code = $result['Code'];
$owner = $result['owner'];
$color = $result['color'];

$result= $conn->query("SELECT ID_user From users_class WHERE ID_class = '$code'");
$i = 0;
$arrStudentInfo = array();
while ($row = $result->fetch_assoc()) {
  $arr = $conn->query("SELECT ID_facebook,Name,Image From users WHERE ID_facebook = ".$row['ID_user'])->fetch_assoc();
  if ($arr['ID_facebook'] != $owner) {
      $result1 = $conn->query("SELECT ID FROM check_user WHERE ID_facebook = ".$row['ID_user']." AND ID_check = '$ID_check'");
      if ($result1->num_rows != 0) {
        $checked = "<span class=greenText>ลงชื่อแล้ว</span>";
      } else {
        $checked = "<span class=redText>ยังไม่ลงชื่อ</span>";
      }
      $arrStudentInfo[$i] = array("name"=>$arr['Name'],"image"=>$arr['Image'],"checked"=>$checked);
      $i++;
  }
}

$r =  $conn->query("SELECT Date,Time From check_class WHERE ID = '$ID_check'")->fetch_assoc();
$arrCheckInfo = array("date"=>$r['Date'],"time"=>$r['Time'],"color"=>$color);

if($color == 1){
  $htmlBody = '#f3e5f5';
  $tableViolet = '#9400D3';
  $checking = '#9400D3';
  $topBar = '#4B0082';
}else if($color == 2){
  $htmlBody = '#F0F8FF';
  $tableViolet = '#00BFFF';
  $topBar = '#1E90FF';
}else if($color == 3){
  $htmlBody = '#ffebee';
  $tableViolet = '#f44336';
  $topBar = '#d50000';
}else if($color == 4){
  $htmlBody = '#e8f5e9';
  $tableViolet = '#00e676';
  $topBar = '#00c853';
}else if($color == 5){
  $htmlBody = '#fff9c4';
  $tableViolet = '#ffeb3b';
  $topBar = '#ffd600';
}else if($color == 6){
  $htmlBody = '#fff3e0';
  $tableViolet = '#ffab40';
  $topBar = '#ff6d00';
}else if($color == 7){
  $htmlBody = '#fce4ec';
  $tableViolet = '#ff4081';
  $topBar = '#f50057';
}

$all = array("checkInfo"=>$arrCheckInfo,"checkStudent"=>$arrStudentInfo,
            "color"=>array("htmlBody"=>$htmlBody,"tableViolet"=>$tableViolet,"topBar"=>$topBar));

echo json_encode($all);
$conn->close();
