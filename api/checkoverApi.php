<?php

header('Content-Type: application/json');
include('../config.php');

$conn = new mysqli($servername, $username, $password, $dbname);

$sql = "SELECT * FROM class";
$results = $conn->query($sql);
$i = 0;
while($row = $results->fetch_assoc()){
  $owner = $conn->query("SELECT Name FROM users WHERE ID_facebook=".$row['owner'])->fetch_assoc();
  $arr[$i] = array('name'=>$row['Name'],'owner' => $owner['Name'], 'status'=>$row['status'], 'จำนวนนักเรียนที่รับ'=>$row['Number_student'],
                  'date'=>$row['Date'],'time'=>$row['Time'],
                'location'=>array('lat'=>$row['lat'],'long'=>$row['longg']));
  $i++;
}

$arrAll = array('checkover'=>$arr);

echo json_encode($arrAll);
$conn->close();
// echo "test";
