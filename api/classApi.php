<?php
// http://localhost/projectxml/api/classApi.php?idClass=37&idUser=1638200376239810&CheckPage=1

header('Content-Type: application/json');
include('../config.php');

$conn = new mysqli($servername, $username, $password, $dbname);

$ID_class = $_GET['idClass'];
$ID_facebook = $_GET['idUser'];
if (!isset($_GET['CheckPage'])) {
    $CheckPage = 1;
    $limitCheckPage = 0;
} else {
    $CheckPage = $_GET['CheckPage'];
    if ($CheckPage==1) {
        $limitCheckPage = 0;
    } else {
        $limitCheckPage = $CheckPage*5-5;
    }
}

$sql = "SELECT Name,Code,owner,status,color FROM class WHERE ID = '$ID_class'";
$row = $conn->query($sql)->fetch_assoc();
$Name_class = $row['Name'];
$owner = $row['owner'];
$code = $row['Code'];
$status = $row['status'];
$color = $row['color'];

$arrClassInfo = array("className"=>$Name_class,"owner"=>$owner,"code"=>$code,"status"=>$status);

$sql = "SELECT ID,Date,Time,Status From check_class WHERE Class_code = '$code' ORDER BY ID DESC LIMIT ".$limitCheckPage.",5";
$result = $conn->query($sql);
$i=0;
while ($row = $result->fetch_assoc()) {
    $ID_checked = $row['ID'];
    $result1 = $conn->query("SELECT ID FROM check_user WHERE ID_facebook = '$ID_facebook' AND ID_check = '$ID_checked'");
    $checked = "";
    if ($owner != $ID_facebook) {
        if ($result1->num_rows != 0) {
            $checked = "<img src=image/correct.png title=ลงชื่อแล้ว width=30px height=30px>";
        } else {
            $checked = "<img src=image/incorrect.png title=ยังไม่ลงชื่อ width=30px height=30px>";
        }
    }
    ($row['Status'] == "open") ? $row['Status'] = "เปิด" : $row['Status'] = "ปิด";
    $arrCheckClass[$i] = array("checked"=>$checked,"date"=>$row['Date'],"time"=>$row['Time'],"ID_check"=>$ID_checked,"status"=>$row['Status'],"checkStatus"=>$result1->num_rows);
    $i++;
}
$numPage = $conn->query("SELECT ID From check_class WHERE Class_code = '$code'")->num_rows;

$result= $conn->query("SELECT ID_user From users_class WHERE ID_class = '$code'");
$numStudentAll = ($result->num_rows)-1;
$arrNumStudent = array();
$i = 0;
if ($numStudentAll > 0) {
    while ($row = $result->fetch_assoc()) {
        $arr = $conn->query("SELECT ID_facebook,Name,Image From users WHERE ID_facebook = ".$row['ID_user'])->fetch_assoc();
        if ($arr['ID_facebook'] != $owner) {
            $arrNumStudent[$i] = array("ID_facebook"=>$arr['ID_facebook'],"Name"=>$arr['Name'],"Image"=>$arr['Image']);
            $i++;
        }
    }
}
if($color == 1){
  $htmlBody = '#f3e5f5';
  $tableViolet = '#9400D3';
  $checking = '#9400D3';
  $topBar = '#4B0082';
  $closeClass = '#9400D3';
}else if($color == 2){
  $htmlBody = '#F0F8FF';
  $tableViolet = '#00BFFF';
  $checking = '#00BFFF';
  $topBar = '#1E90FF';
  $closeClass = '#00BFFF';
}else if($color == 3){
  $htmlBody = '#ffebee';
  $tableViolet = '#f44336';
  $checking = '#f44336';
  $topBar = '#d50000';
  $closeClass = '#f44336';
}else if($color == 4){
  $htmlBody = '#e8f5e9';
  $tableViolet = '#00e676';
  $checking = '#00e676';
  $topBar = '#00c853';
  $closeClass = '#00e676';
}else if($color == 5){
  $htmlBody = '#fff9c4';
  $tableViolet = '#ffeb3b';
  $checking = '#ffeb3b';
  $topBar = '#ffd600';
  $closeClass = '#ffeb3b';
}else if($color == 6){
  $htmlBody = '#fff3e0';
  $tableViolet = '#ffab40';
  $checking = '#ffab40';
  $topBar = '#ff6d00';
  $closeClass = '#ffab40';
}else if($color == 7){
  $htmlBody = '#fce4ec';
  $tableViolet = '#ff4081';
  $checking = '#ff4081';
  $topBar = '#f50057';
  $closeClass = '#ff4081';
}
$all = array("ClassInfo"=>$arrClassInfo,
            "CheckClass"=>array("numPage"=>$numPage,"checkInfo"=>$arrCheckClass),
            "numberStudent"=>array("numStudentAll"=>$numStudentAll,"table"=>$arrNumStudent),
            "color"=>array("htmlBody"=>$htmlBody,"tableViolet"=>$tableViolet,"checking"=>$checking,"topBar"=>$topBar,"closeClass"=>$closeClass));
echo json_encode($all);
$conn->close();
