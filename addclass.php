<?php
session_start();
if (!isset($_SESSION['ID_facebook'])) {
    header("location:index.html");
}
?>
<!DOCTYPE html>
<html>

    <head>
        <title>CheckOver</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Security-Policy" content="default-src *; script-src 'self' 'unsafe-inline' 'unsafe-eval' *; style-src 'self' 'unsafe-inline' *; img-src * data: 'unsafe-inline'">
        <link rel="icon" href="image/logo.png">
        <link rel="stylesheet" type="text/css" href="css/addclass.css">
        <link rel="stylesheet" type="text/css" href="css/topbar.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <body >
        <div class="container-fluid topBar">
          <div class="profile topbarContent">
            <a href="user_info.php"><img class="imgProfile" src="<?=$_SESSION['Image_user'] ?>"></a>
          </div>
          <div class="name topbarContent"><?php echo $_SESSION['Name_user'];?></div>
          <div class="plus topbarContent" title = "ออกจากระบบ" id = "logoutPic"><a href="logout.php"><img src="image/logout.png" width="100%"></a>
            </div>
        </div>
        <div class="container-fluid bodyPdTop">
            <div class="code">
                <form action="connectsql/AddclassAction.php" method="post" onsubmit="return check_code();">
                    ใส่ Code เพื่อเข้าร่วมคลาส
                    <input type="text" name="code_class" id="code_class" class="blackText">
                    <button type="submit" class="myButton">ตกลง</button>
                    <p id="codeC_p" class="alertTxt"/>
                </form>
            </div>
            <div class="classcreate">
                <form action="connectsql/AddclassAction.php" method="post" onsubmit="return check();">
                    <table width="100%">
                      <tr>
                          <th> ชื่อชั้นเรียน:<span class="alertTxt">*</span> </th>
                          <th> <input type="text" name="Cname" value="" id="Cname" class="blackText lengthInput"/><p id="Cname_p" class="alertTxt"></p></th>
                      </tr>
                        <tr>
                            <th> จำนวนครั้งที่เรียน:<span class="alertTxt">*</span> </th>
                            <th> <input type="number" name="CH" value="" id="CH" class="blackText lengthInput"/><p id="CH_p" class="alertTxt"></p></th>
                        </tr>

                        <tr>
                            <th> จำนวนนักเรียน:<span class="alertTxt">*</span>   </th>
                            <th><input type="number" name="Stu" value="" id="Stu"  class="blackText lengthInput"/><p id="Stu_p" class="alertTxt"></p></th>
                        </tr>

                        <tr>
                            <th> วันเริ่มต้น:<span class="alertTxt">*</span>   </th>
                            <th><input type="date" name="StaD" value="" id="StaD"  class="blackText lengthInput"/><p id="StaD_p" class="alertTxt"></p></th>
                        </tr>

                        <tr>
                            <th> วันสิ้นสุด:<span class="alertTxt">*</span>   </th>
                            <th><input type="date" name="EndD" value="" id="EndD"  class="blackText lengthInput"/><p id="EndD_p" class="alertTxt"></p></th>
                        </tr>
                        <tr>
                          <th>
                            <input type="checkbox" name="day[]" value="1" checked>วันจันทร์<br>
                            <input type="checkbox" name="day[]" value="1">วันอังคาร<br>
                            <input type="checkbox" name="day[]" value="1">วันพุธ<br>
                            <input type="checkbox" name="day[]" value="1">วันพฤหัสบดี<br>
                            <input type="checkbox" name="day[]" value="1">วันศุกร์<br>
                            <input type="checkbox" name="day[]" value="1">วันเสาร์<br>
                            <input type="checkbox" name="day[]" value="1">วันอาทิตย์<br>
                          </th>
                          <th>
                            เวลาเริ่มเรียน<span class="alertTxt">*</span><input type="time" name="time1" id="time1" class="blackText lengthInput"><p id="time1_p" class="alertTxt"></p><br>
                            เวลาเลิกเรียน<span class="alertTxt">*</span><input type="time" name="time2" id="time2" class="blackText lengthInput"><p id="time2_p" class="alertTxt"></p><br>
                          </th>
                        </tr>

                        <tr>
                          <th>
                                <button type="submit" class="myButton">ตกลง</button><br/>
                                <p id="location_p" class="alertTxt"></p>
                                <input type="text" value="" hidden="TRUE" id="code" name="code">
                                <input type="text" value="" hidden="TRUE" id="lat" name="lat" >
                                <input type="text" value="" hidden="TRUE" id="longg" name="longg" >
                            </th>
                            <th>

                            </th>
                        </tr>
                    </table>

                    <p id="locc"></p>
                    <p id="locc2"></p>
                </form>
            </div>
            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
            <div id="map"></div>
        </div>
        <script src="script/addclass.js">
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLhmjstMug2_3HbbGoxupPdP6j__j_RYg&callback=initMap&libraries=places">
        </script>


    </body>

</html>
