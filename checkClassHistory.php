<?php
  session_start();
  if (!isset($_SESSION['ID_facebook'])) {
      header("location:index.php");
  }
  $url = 'http://localhost/~checkove/api/checkClassHistoryApi.php?idClass='.$_GET['idClass'].'&idCheck='.$_GET['idCheck'];
  $json = file_get_contents($url);
  $arr = json_decode($json);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ประวัติการเช็คชื่อของ <?=$arr->userInfo->Name?></title>
    <link rel="icon" href="image/logo.png">
    <link rel="stylesheet" type="text/css" href="css/checkClassHistory.css">
    <link rel="stylesheet" type="text/css" href="css/topbar.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style media="screen">
      .topBar{
        background-color: <?=$arr->color->topBar?>;
      }
      html, body{
        background-color: <?=$arr->color->htmlBody?> !important;
      }
      .tableViolet{
        background-color:	<?=$arr->color->tableViolet?>;
      }
    </style>
  </head>
  <body>
    <div class="container-fluid topBar">
      <div class="profile topbarContent">
        <a href="user_info.php"><img class="imgProfile" src="<?=$_SESSION['Image_user']?>"></a>
      </div>
      <div class="name topbarContent"><?=$_SESSION['Name_user']?></div>
      <div class="plus topbarContent"><a href="logout.php"><img src="image/logout.png" width="100%" title="ออกจากระบบ"></a></div>
    </div>
    <div class="container-fluid bodyPdTop" id="bodyContainer">
    <table class=tableViolet>
      <tr>
        <th >
          <a href="class.php?id=<?=$_GET['idClass']?>"><img src="image/Left-Arrow-PNG-File.png" width="50px" height="50px" class="pdleft"  title="ย้อนกลับ"></a><div class=numberStudent> วันที่ <?=$arr->checkInfo->date?> เวลา <?=$arr->checkInfo->time?></div></th>
      </tr>
      <?php foreach ($arr->checkStudent as $checkStudent){
        echo "<tr><td class='tdViolet'><img class=imgStudent src=".$checkStudent->image."> <div class=fontDT>".$checkStudent->name." ".$checkStudent->checked."</div></td></tr>";
      }
      ?>
    </table>
  </div>
  </body>
</html>
