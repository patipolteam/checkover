<?php
  session_start();
  if (!isset($_SESSION['ID_facebook'])) {
      header("location:index.php");
  }
      $ID_class = $_GET['id'];
      $ID_facebook = $_SESSION['ID_facebook'];
      if(!isset($_GET['CheckPage'])) $_GET['CheckPage'] = 1;
      $url = 'http://localhost/~checkove/api/classApi.php?idClass='.$_GET['id'].'&idUser='.$_SESSION['ID_facebook'].'&CheckPage='.$_GET['CheckPage'];
      $json = file_get_contents($url);
      $arr = json_decode($json);
      $ClassInfo = $arr->ClassInfo;
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="image/logo.png">
    <link rel="stylesheet" type="text/css" href="css/class.css">
    <link rel="stylesheet" type="text/css" href="css/topbar.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
      var statusClass = '<?=$ClassInfo->status?>'
      var ID_class = '<?=$ID_class?>'
    </script>
    <script src="script/class.js"></script>
    <style media="screen">
      .topBar{
        background-color: <?=$arr->color->topBar?>;
      }
      html, body{
        background-color: <?=$arr->color->htmlBody?> !important;
      }
      .checking{
        background-color:	<?=$arr->color->checking?>;
      }
      .tableViolet{
        background-color:	<?=$arr->color->tableViolet?>;
      }
      .closeClass{
        background-color: <?=$arr->color->closeClass?>;
      }
    </style>

    <title>ยินดีต้อนรับสู่คลาส <?=$ClassInfo->className?></title>
  </head>
  <body>
    <div class="container-fluid topBar" id="tobBar">
      <div class="profile topbarContent"><a href="user_info.php"><img class="imgProfile" src="<?=$_SESSION['Image_user']?>"></a></div>
      <div class="name topbarContent"><?=$ClassInfo->className?></div>
      <div class="plus topbarContent"><a href="logout.php"><img src="image/logout.png" width="100%" height="auto" title="ออกจากระบบ"></a></div>
      <!-- <div class="plus topbarContent"><a href="#"><img src="image/descriptionClass.png" width="100%" height="auto" title="รายละเอียดคลาส"></a></div> -->
      <?php if($ClassInfo->owner == $ID_facebook) {?>
      <div class="topbarContent other">
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">จัดการคลาส
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
          <li><a href="#" id="openclose">เปิด/ปิดคลาส</a></li>
          <li><a href="#" id="deleteClass">ลบคลาส</a></li>
          <li><a href="connectsql/createcheck.php?code=<?=$ClassInfo->code?>&id=<?=$ID_class?>">เพิ่มการเช็คชื่อ</a></li>
          <li class="divider"></li>
          <li class="text-center"><?=$ClassInfo->code?></li>
        </ul>
        </div>
      </div>
      <?php } ?>
    </div>
    <div class="container-fluid bodyPdTop">
        <?php
        if($ID_facebook == $ClassInfo->owner || $ClassInfo->status == "open"){
          foreach ($arr->CheckClass->checkInfo as $checkInfo) {
            echo "<div class=checking><div class=mgFontChecking>วันที่:$checkInfo->date เวลา:$checkInfo->time $checkInfo->checked</div>";
            if ($ClassInfo->owner!=$ID_facebook && $checkInfo->status=="เปิด" && $checkInfo->checkStatus == 0) {
              echo "<a href=connectsql/check_user.php?id=".$ID_class."&ID_check=".$checkInfo->ID_check."><img src=image/checkmark.png class=imgChecking title=ลงชื่อ></a>";
            } elseif($ClassInfo->owner==$ID_facebook){
              echo "<a href=checkClassHistory.php?idClass=".$ID_class."&idCheck=".$checkInfo->ID_check." class='checkStatus'>ประวัติ</a>";
              if($checkInfo->status=="เปิด"){
                $txt = "ปิด";
              }else{
                $txt = "เปิด";
              }
              echo "<a href=connectsql/close_check.php?id=".$ID_class."&ID_check=".$checkInfo->ID_check." class='checkStatus mgRightCheck'>".$txt."</a>";
            }
            echo "</div>";
          }
          if($arr->CheckClass->numPage>5){
            $left_arrow = "";
            $right_arrow = "";
            $left_a = "<span>«</span>";
            $right_a = "<span>»</span>";
            if($_GET['CheckPage']==1){
              $left_arrow = "class=disabled";
              $right_arrow = "";
              $right_a = "<a href=class.php?id=".$ID_class."&CheckPage=".($_GET['CheckPage']+1).">»</a>";
            }else if($_GET['CheckPage']==ceil($_GET['CheckPage']/5)){
              $left_arrow = "";
              $left_a = "<a href=class.php?id=".$ID_class."&CheckPage=".($_GET['CheckPage']-1).">«</a>";
              $right_arrow = "class=disabled";
            }else{
              $left_a = "<a href=class.php?id=".$ID_class."&CheckPage=".($_GET['CheckPage']-1).">«</a>";
              $right_a = "<a href=class.php?id=".$ID_class."&CheckPage=".($_GET['CheckPage']+1).">»</a>";
            }
            echo "<div class=text-center>
            <div class=page-nation>
            <ul class=pagination pagination-large>
            <li ".$left_arrow.">".$left_a."</li>";
            for($i=1;$i<=ceil($arr->CheckClass->numPage/5);$i++){
              if($i==$_GET['CheckPage']){
                echo "<li class=active><span>".$i."</span></li>";
              }else{
                echo "<li><a href=class.php?id=".$ID_class."&CheckPage=".$i.">".$i."</a></li>";
              }
            }
            echo "<li ".$right_arrow.">".$right_a."</li>
            </ul>
            </div>
            </div>";
          }
          echo "<table class=tableViolet><tr><th class=numberStudent>จำนวนนักเรียน ".$arr->numberStudent->numStudentAll." คน</th></tr>";
          foreach ($arr->numberStudent->table as $table){
            echo "<tr><td class=tdViolet><div class=profile><img class=imgProfile src=".$table->Image."></div>";
            echo "<div class=nameStudent>".$table->Name."</div>";
            if($ClassInfo->owner==$ID_facebook){
              echo "<a href=checkHistory.php?idClass=".$ID_class."&id=".$table->ID_facebook." class='checkStatus pdHistory'>ประวัติการเช็คชื่อ</a>";
            }
            echo "</td></tr>";
          }
        }else{
          echo "<div class=closeClass>คลาสปิดแล้ว</div>";
        }
        ?>
    </div>
  </body>
</html>
