<?php
  session_start();
  if (!isset($_SESSION['ID_facebook'])) {
      header("location:index.php");
  }
  $url = 'http://localhost/~checkove/api/userInfoApi.php?id='.$_SESSION['ID_facebook'];
  $json = file_get_contents($url);
  $arr = json_decode($json);
?>
<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="image/logo.png">
  <link rel="stylesheet" type="text/css" href="css/userinfo.css">
  <link rel="stylesheet" type="text/css" href="css/topbar.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <title>Welcome <?=$_SESSION['Name_user']?></title>
</head>

<body>
  <div class="container-fluid topBar">
    <div class="profile topbarContent">
      <a href="#"><img class="imgProfile" src="<?=$_SESSION['Image_user']?>" width="60px" height="60px"></a>
    </div>
    <div class="name topbarContent"><?=$_SESSION['Name_user']?></div>
    <div class="plus topbarContent" id = "addClassPic" title = "สร้าง Class ใหม่ หรือ เข้าร่วม Class"><a href="addclass.php"><img src="image/plus.png" width="100%"></a>
    </div>
    <div class="plus topbarContent" id = "logoutPic" title = "ออกจากระบบ"><a href="logout.php"><img src="image/logout.png" width="100%"></a>
    </div>
    </div>
  <div class="container-fluid bodyPdTop">
  <?php
    foreach ($arr->Class as $Class) {
      echo "<div class=box style='background-image: url(image/".$Class->Color.".jpg)';>";
      echo "<div class=boxTopicc>";
      if(strlen($Class->title) > 40){
        $out =  iconv_substr($Class->title,0,40,"UTF-8")."...";
        echo "<a href=class.php?id=$Class->id class=boxTopic title=$Class->title>$out</a>";

      } else{
        echo "<a href=class.php?id=$Class->id class=boxTopic title=$Class->title>$Class->title</a>";

      }

      echo "</div>";
      echo "<div class=boxImg><img class=ownerClass src=$Class->img></div>";
      echo "</div>";
    }
   ?>
  </div>
</body>

</html>
