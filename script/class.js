$(document).ready(function() {
  // $.getJSON('http://localhost/projectxml/api/classApi.php?idClass=' + ID_class + '&idUser' + ID_facebook + '&CheckPage=' + CheckPage, function(json) {
  //   var classInfo = json.ClassInfo;
  //   if (classInfo.owner == ID_facebook) {
  //     $("#topBar").append("<div class=topbarContent other>" +
  //       "<div class=dropdown>" +
  //       "<button class='btn btn-default dropdown-toggle' type=button data-toggle=dropdown>อื่นๆ" +
  //       "<span class=caret></span></button>" +
  //       "<ul class=dropdown-menu>" +
  //       "<li><a href=#>เปิด/ปิดคลาส</a></li>" +
  //       "<li><a href=createcheck.php?code=" + classInfo.code + "&id=" + ID_class + ">เพิ่มการเช็คชื่อ</a></li>" +
  //       "<li class=divider></li>" +
  //       "<li class=text-center>" + classInfo.code + "</li>" +
  //       "</ul>" +
  //       "</div>" +
  //       "</div>")
  //   }
  // });
  $("#openclose").click(function(event) {
    var txt
    var status
    if(statusClass == "open"){
      txt = "คุณต้องการจะปิดคลาสใช่หรือไม่"
      status = "close"
    }else{
      txt = "คุณต้องการจะเปิดคลาสใช่หรือไม่"
      status = "open"
    }
    var r = confirm(txt)
    if (r == true) {
      $.ajax({
        type: 'GET',
        url: 'connectsql/opencloseClass.php',
        data: {
          id: ID_class,
          status: status
        },
        success: function(data) {
          location.reload();
        }
      });
    }
  });
  $("#deleteClass").click(function(event) {
    var r = confirm("คุณต้องการจะลบคลาสใช่หรือไม่")
    if (r == true) {
      $.ajax({
        type: 'GET',
        url: 'connectsql/deleteClass.php',
        data: {
          id: ID_class
        },
        success: function(data) {
          if (data == "success") {
            window.location.replace("user_info.php");
          }
        }
      });
    }
  });
});
