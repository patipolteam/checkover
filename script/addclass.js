function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 8; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  document.getElementById('code').value = text;
  return false;
}

function check() {
  makeid();
  if (document.getElementById("Cname").value == "") {
    clear();
    document.getElementById("Cname_p").innerHTML = "*กรุณาใส่ชื่อชั้นเรียน";
  } else if (document.getElementById("CH").value == "") {
    clear();
    document.getElementById("CH_p").innerHTML = "*กรุณาใส่จำนวนครั้งที่เรียน";
  } else if (document.getElementById("Stu").value == "") {
    clear();
    document.getElementById("Stu_p").innerHTML = "*กรุณาใส่จำนวนนักเรียน";
  } else if (document.getElementById("StaD").value == "") {
    clear();
    document.getElementById("StaD_p").innerHTML = "*กรุณาเลือกวันเริ่มเรียน";
  } else if (document.getElementById("EndD").value == "") {
    clear();
    document.getElementById("EndD_p").innerHTML = "*กรุณาเลือกวันสิ้นสุด";
  } else if (document.getElementById("time1").value == "") {
    clear();
    document.getElementById("time1_p").innerHTML = "*กรุณาเลือกเวลาเริ่มเรียน";
  } else if (document.getElementById("time2").value == "") {
    clear();
    document.getElementById("time2_p").innerHTML = "*กรุณาเลือกเวลาเลิกเรียน";
  } else if (document.getElementById("lat").value == "" | document.getElementById("longg").value == "") {
    clear();
    document.getElementById("location_p").innerHTML = "*กรุณาเลือกสถานที่ตั้ง";
  } else {
    return true;
  }
  return false;
}

function check_code() {
  if (document.getElementById("code_class").value == "") {
    document.getElementById("codeC_p").innerHTML = "*กรุณาใส่ Code";
  } else {
    return true;
  }
  return false;
}

function clear() {
  document.getElementById("Cname_p").innerHTML = "";
  document.getElementById("CH_p").innerHTML = "";
  document.getElementById("Stu_p").innerHTML = "";
  document.getElementById("StaD_p").innerHTML = "";
  document.getElementById("EndD_p").innerHTML = "";
  document.getElementById("time1_p").innerHTML = "";
  document.getElementById("time2_p").innerHTML = "";
  document.getElementById("location_p").innerHTML = "";
  return false;
}

// In the following example, markers appear when the user clicks on the map.
// The markers are stored in an array.
// The user can then click an option to hide, show or delete the markers.
var map;
var markers = [];
var latt = "";
var longg = "";

getLocation();

function getLocation() {
  {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else {
      document.getElementById("locc").innerHTML = "Geolocation is not supported by this browser.";
    }

  }
}

function showPosition(position) {
  latt = position.coords.latitude;
  longg = position.coords.longitude;
  document.getElementById("locc").innerHTML = "Your current location <br/> lat:" + Number(latt) + "  long:" + Number(longg);
  initMap()
}


function initMap() {
  var haightAshbury = {
    lat: 16.44671,
    lng: 102.833
  };

  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 12,
    center: haightAshbury,
    mapTypeId: 'terrain'
  });


  // This event listener will call addMarker() when the map is clicked.
  map.addListener('click', function(event) {
    addMarker(event.latLng);
    document.getElementById("locc2").innerHTML = "Marker location is <br/> lat:" + event.latLng.lat() + " lng: " + event.latLng.lng();
    document.getElementById("lat").value = event.latLng.lat();
    document.getElementById("longg").value = event.latLng.lng();
  });

  // Adds a marker at the center of the map.
  addMarker(haightAshbury);


//SearchBox
var input = document.getElementById('pac-input');
var searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        searchBox.addListener('places_changed', function() {
                  var places = searchBox.getPlaces();

                  if (places.length == 0) {
                    return;
                  }

                  // Clear out the old markers.
                  markers.forEach(function(marker) {
                    marker.setMap(null);
                  });
                  markers = [];

                  // For each place, get the icon, name and location.
                  var bounds = new google.maps.LatLngBounds();
                  places.forEach(function(place) {
                    if (!place.geometry) {
                      console.log("Returned place contains no geometry");
                      return;
                    }
                    var icon = {
                      url: place.icon,
                      size: new google.maps.Size(71, 71),
                      origin: new google.maps.Point(0, 0),
                      anchor: new google.maps.Point(17, 34),
                      scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                      map: map,
                      icon: icon,
                      title: place.name,
                      position: place.geometry.location
                    }));
                    latt = place.geometry.location.lat();
                    longg = place.geometry.location.lng();
                    document.getElementById("locc2").innerHTML = "Marker location is <br/> "+ place.name +"<br/>ที่อยู่: " + place.formatted_address + "<br/>lat:" + latt + " lng: " + longg ;
                    document.getElementById("lat").value = latt;
                    document.getElementById("longg").value = longg;
                    if (place.geometry.viewport) {
                      // Only geocodes have viewport.
                      bounds.union(place.geometry.viewport);
                    } else {
                      bounds.extend(place.geometry.location);
                    }
                  });
                  map.fitBounds(bounds);
                });


}

// Adds a marker to the map and push to the array.
function addMarker(location) {
  deleteMarkers();
  var marker = new google.maps.Marker({
    position: location,
    map: map
  });
  markers.push(marker);
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() {
  setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function currentlocation() {
  var haightAshbury = {
    lat: Number(latt),
    lng: Number(longg)
  };
  addMarker(haightAshbury);

}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}
