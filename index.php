<?php
session_start();
    if (isset($_SESSION['ID_facebook'])) {
        header("location:user_info.php");
    }
?>
<!doctype html>
<html lang="en">

<head>
  <title>ยินดีต้อนรับสู่ CheckOver</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="css/index.css">
  <script src="script/index.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="icon" href="image/logo.png">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>
  <div class="bgimage"></div>
  <div class="center">
    <div class="br"><img src="image/logo.png" width="200px" height="200px" /></div>
    <div id="fb-root"></div>
    <div class="fb-login-button" onlogin="checkLoginState();" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>

  </div>
</body>

</html>
